/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

/**
 *
 * @author andrebsguedes
 */
public class PessoaFisica extends Pessoa {
    private String cpf;
    private String cargo;

    public PessoaFisica(String nome, String telefone, String endereco, String email, String cpf, String cargo) {
        super(nome,telefone,endereco,email);
        this.cpf = cpf;
        this.cargo = cargo;
    }
    
    public PessoaFisica(){
    
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    

    
    
    
}
