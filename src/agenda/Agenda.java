/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author alunos
 */
public class Agenda {
    
    private Vector listaContatos;
    
    public Agenda(){
        listaContatos = new Vector();     
    }
    
    public String adicionarPessoa(PessoaFisica fisica){
        listaContatos.add(fisica);
        return "Pessoa Fisica adicionada com sucesso!";
    
    }
    public String adicionarPessoa(PessoaJuridica juridica){
        listaContatos.add(juridica);
        return "Pessoa Juridica adicionada com sucesso!";
    
    }

}
