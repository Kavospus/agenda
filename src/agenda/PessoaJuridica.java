/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package agenda;

/**
 *
 * @author andrebsguedes
 */
public class PessoaJuridica extends Pessoa{
    private String cnpj;
    private String razaoSocial;

    public PessoaJuridica(String cnpj, String razaoSocial, String nome, String telefone, String endereco, String email) {
        super(nome, telefone, endereco, email);
        this.cnpj = cnpj;
        this.razaoSocial = razaoSocial;
    }
    
    public PessoaJuridica(){
    
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }
}
